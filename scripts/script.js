function getMovies(){
   let movies; 
   let request = new XMLHttpRequest();

   request.open('GET', 'https://yts.mx/api/v2/list_movies.json');

   request.onreadystatechange = function(){
       if(this.readyState == 4 && this.status == 200){
           movies = JSON.parse(this.response).data.movies;
           createCards('homepage-cards', movies);
       }
       else if(this.status == 404){
           alert('URL not found!')
       }
   };

    request.send();
}
getMovies();

function createCards(container_id, movies){
    for(let i = 0; i < movies.length; i++){
        createCard(container_id,movies[i], i);
    }
}


function createCard(container_id, movie, index){

    //Creating a div for the card holder
    let cardHolder = document.createElement('div');
    cardHolder.classList.add('col-3');
    cardHolder.classList.add('card-holder');

    //Creating a div for the movie card
    let movieCard = document.createElement('div');
    movieCard.classList.add('movie-card');
    movieCard.id = "card-" + index;

    movieCard.style.backgroundImage = "url(" + movie.medium_cover_image + ")";

    movieCard.addEventListener('mouseover', 
        function(){
            showcardcontent('card-content-'+ index, "card-" + index)
        }
    );
    movieCard.addEventListener('mouseleave', 
        function(){
            hidecardcontent('card-content-'+ index, "card-" + index)
        }
    );

    //Creating a div for the card content
    let cardContent= document.createElement('div');
    cardContent.classList.add('card-content');
    cardContent.id = "card-content-" + index;
    
    //Creating a paragraph for the star icon holder
    let starIconHolder = document.createElement('p');
    starIconHolder.classList.add('star-icon-holder');

    //Creating the star icon
    let starIcon = document.createElement('i');
    starIcon.classList.add('fas');
    starIcon.classList.add('fa-star');

    //Append the icon to the icon holder
    starIconHolder.append(starIcon);
    
    //Creating a paragraph for the rating
    let rating = document.createElement('p');
    rating.classList.add('rating');
    rating.innerText = movie.rating + " / 10";
    
    let viewDetailsBtn = document.createElement('button');
    viewDetailsBtn.classList.add("btn");
    viewDetailsBtn.classList.add("btn-success");
    viewDetailsBtn.classList.add("details-btn");
    viewDetailsBtn.innerText = "View Details";
    viewDetailsBtn.addEventListener('click',
        function() {
            viewDetails('moviedetails.html?movie_id=' + movie.id);
        }
    );

    cardContent.append(starIconHolder);
    cardContent.append(rating);

    if(movie.genres){
        movie.genres.forEach(Genre => {
            
            let genre = document.createElement('p');
            genre.classList.add('genre');
            genre.innerText = Genre;
            
            cardContent.append(genre);
        });
    }

    let movieName = document.createElement('p');
    movieName.innerText = movie.title + ' (' + movie.year + ')';
    movieName.classList.add('movie-card-title');
    movieName.addEventListener('click',
        function() {
            viewDetails('moviedetails.html?movie_id=' + movie.id);
        }
    );

    cardContent.append(viewDetailsBtn);
    //cardContent.append(link);

    movieCard.append(cardContent);

    cardHolder.append(movieCard);
    cardHolder.append(movieName);

    let cardContainer = document.getElementById(container_id);

    cardContainer.append(cardHolder);

}

function showcardcontent(content_id, card_id) {
    let content = document.getElementById(content_id);
    let card = document.getElementById(card_id);

    content.style.display = "block";
    card.style.borderColor = "yellow";
}

function hidecardcontent(content_id, card_id) {
    let content = document.getElementById(content_id);
    let card = document.getElementById(card_id);

    content.style.display = "none";
    card.style.borderColor = "white";
}

function viewDetails(url){
    window.location = url;
}
