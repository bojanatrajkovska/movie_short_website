let urlParameters = new URLSearchParams(window.location.search);
let movie_id = urlParameters.get('movie_id');

getMovie(movie_id);
getSuggestedMovies(movie_id);

function getMovie(movie_id) {

    let movie;
    let request = new XMLHttpRequest();

    request.open('GET', 'https://yts.mx/api/v2/movie_details.json?movie_id=' + movie_id);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            movie = JSON.parse(this.response).data.movie;
            document.title = movie.title;
            document.getElementById('image-holder').style.backgroundImage = "url(" + movie.background_image + ")";
            console.log(movie);
            
            GetTrailer(movie);
            SetTorrents(movie);
        }
        else if (this.status == 404) {
            alert('URL not found!')
        }
    };

    request.send();
}

function getSuggestedMovies(movie_id) {
    ;

    let request = new XMLHttpRequest();

    request.open('GET', 'https://yts.mx/api/v2/movie_suggestions.json?movie_id=' + movie_id);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let suggestedMovies = JSON.parse(this.response).data.movies;

            for (let i = 0; i < suggestedMovies.length; i++) {
                let suggested = document.getElementById('suggested-' + (i + 1));
                let suggestedLink = document.getElementById('s-link-' + (i + 1));

                if (suggested != null && suggested != undefined) {
                    suggested.setAttribute('src', suggestedMovies[i].medium_cover_image)
                    suggestedLink.setAttribute('href', 'moviedetails.html?movie_id=' + suggestedMovies[i].id)
                }
            }
        }
        else if (this.status == 404) {
            alert('URL not found!')
        }
    };

    request.send();
}

function GetTrailer(movie) {
    document.getElementById("movie-image").setAttribute("src", movie.large_cover_image);
    document.getElementById("movie-title").innerText = movie.title;
    document.getElementById("movie-year").innerText = movie.year;
    document.getElementById('movie-trailer').setAttribute('src', 'https://www.youtube.com/embed/' + movie.yt_trailer_code);
    document.getElementById('movie-img-1').setAttribute('src', 'https://img.youtube.com/vi/' + movie.yt_trailer_code + '/1.jpg');
    document.getElementById('movie-img-2').setAttribute('src', 'https://img.youtube.com/vi/' + movie.yt_trailer_code + '/2.jpg');
}
